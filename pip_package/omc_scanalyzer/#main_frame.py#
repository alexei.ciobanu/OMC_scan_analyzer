import copy
import csv

import threading
import sys
if sys.version_info[0] < 3:
    import Tkinter as tk
    import Queue as queue
else:
    import tkinter as tk
    import queue

import numpy as np
import matplotlib as mpl
mpl.use("TkAgg") # needs to be there
from matplotlib.backends import backend_tkagg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.widgets import RadioButtons, RectangleSelector

class namespace:
    pass
        
class RadioOption(tk.Frame):
    def __init__(self, parent, options, command=None, default=None, *args, **kwargs):
        ''' 
        options must be a list of strings and each string has to be a valid python variable name
        '''
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.options = options
        self.default = default
        self.command = command
        
        self.var = tk.StringVar(master=self.parent)
        
        if default is not None:
            self.var.set(default) # select default option
                 
        for option in self.options:
            rb_str = 'rb_'+option
            setattr(self,rb_str,tk.Radiobutton(
            master=self, text=option, variable=self.var, value=option,command=command))
            getattr(self,rb_str).pack(side='top',anchor='w')
                
class OptionsParser(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        
        self.button = tk.Button(master=self, text='Submit', command=self.submit)
        self.button.pack(side='left')
        
        self.entry = tk.Entry(master=self,width=500)
        self.entry.pack(side='left')
              
    def submit(self):
        todict = lambda **kwargs: kwargs
        text = self.entry.get()
        in_dict = eval('todict('+text+')')
        for key in in_dict:
            setattr(self.parent, key, in_dict[key])
        
class KillButton(tk.Frame):
    def __init__(self, parent, target, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.target = target
        
        self.button = tk.Button(master=self, text='Kill', command=self.Kill)
        self.button.pack(side='left')
        
    def Kill(self):
        self.target.destroy()
        
class GraphFrame(tk.Frame):
    def __init__(self, parent, graph, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.graph = graph
        self.parent = parent
        
        self.canvas = FigureCanvasTkAgg(self.graph, master=self).get_tk_widget()
        self.graph.canvas.draw()
        self.canvas.pack(side="left")
        
class MyFigure(mpl.figure.Figure):
    def __init__(self, *args, **kwargs):
        mpl.figure.Figure.__init__(self, *args, **kwargs)
        self.ax = self.add_subplot(111)
        self.line, = self.ax.plot([],[])
        
class DoubleSubplot(mpl.figure.Figure):
    def __init__(self, *args, **kwargs):
        mpl.figure.Figure.__init__(self, *args, **kwargs)
        self.ax1 = self.add_subplot(211)
        self.line1, = self.ax1.plot([],[])
        self.ax2 = self.add_subplot(212)
        self.line2, = self.ax2.plot([],[])
        
# class MyRectSelec(mpl.widgets.RectangleSelector):
    # def __init__(self, *args, **kwargs):
        # mpl.widgets.RectangleSelector.__init__(self, *args, **kwargs)
        
class PlotWindow(tk.Toplevel):
    def __init__(self, *args, **kwargs):
        tk.Toplevel.__init__(self, *args, **kwargs)
        
    def destroy(self):
        'destroying the plot window should also turn off rectselects'
        self.master.graph1.ax1.RS1.set_visible(False)
        self.master.graph1.ax1.RS2.set_visible(False)
        tk.Toplevel.destroy(self)
        
class MainApplication(tk.Frame):
    # the parent of this must be of type tkinter.Tk (i.e. have mainloop() and after())
    def __init__(self, parent, omc_chan, pzt_chan, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        
        self.parent = parent
        self.omc_chan = omc_chan
        self.pzt_chan = pzt_chan
        self.queue = queue.Queue()
        self.worker = threading.Thread(target=self.pull_channels_loop)
        self.worker.daemon = True
        
        ########################################################################################
        
        self.duration = 12
        self.q = 8
        self.pzt_lim = [-10,120]
        self.omc_lim = [1e-5,0.6]
        self.yscale = 'linear'
        self.lowpass = False
        self.pzt_filter = True
        self.kill_time = 1
        self.dark_level = -2e-4
        self.draw_speed = 0.5
        self.valid_boxes = False
        self.m_range = 500
        
        self.zeroth = namespace()
        self.zeroth.x = [0,0]
        self.zeroth.y = [0,0]
        
        self.second = namespace()
        self.second.x = [0,0]
        self.second.y = [0,0]
        
        self.mismatches = []
        self.t_mismatches = []
        
        ########################################################################################
        
        self.graph0 = DoubleSubplot()
        self.graph0.ax1.set_ylabel('PZT voltage [counts]')
        self.graph0.ax2.set_xlabel('Time [sec]')
        self.graph0.ax2.set_ylabel('OMC DCPD [counts]') 
        
        self.graph1 = DoubleSubplot()
        self.graph1.ax1.set_xlabel('PZT voltage [counts]')
        self.graph1.ax1.set_ylabel('OMC DCPD [counts]') 
        self.graph1.ax2.set_xlabel('Time [sec]')
        self.graph1.ax2.set_ylabel('$P_2\ / \ P_1 $')
        
        self.graph_group = tk.Frame(self)
        
        initial_text = 'duration={0},pzt_lim={1},omc_lim={2},q={3},kill_time={4},dark_level={5},draw_speed={6},m_range={7}'\
    .format(self.duration,self.pzt_lim,self.omc_lim,self.q,self.kill_time,self.dark_level,self.draw_speed,self.m_range)
        self.optionsparser = OptionsParser(self)
        self.optionsparser.entry.insert(0, initial_text)
        
        self.killbutton = KillButton(self,target=self.parent)
        self.pop0button = tk.Button(master=self,text='pop 0',command=self.pop_0)
        self.pop1button = tk.Button(master=self,text='pop 1',command=self.pop_1)
        self.graphframe0 = GraphFrame(self.graph_group,graph=self.graph0)
        self.graphframe1 = GraphFrame(self.graph_group,graph=self.graph1)
        self.rb_yscale = RadioOption(self,['linear','semilogy'],default='linear')
        self.rb_pzt_filter = RadioOption(self,['filter','no_filter'],default='filter')
        
        self.optionsparser.pack(side="top",fill="both", expand=True)
        self.killbutton.pack(side="top",fill="both", expand=True)
        self.pop0button.pack(side="top",anchor='w')
        self.pop1button.pack(side="top",anchor='w')
        # self.graphframe0.pack(side="left", fill='both', expand=True)
        # self.graphframe1.pack(side="left", fill='both', expand=True)
        # self.graph_group.pack(side="top", fill='both', expand=True)
        self.rb_yscale.pack(side="left")
        self.rb_pzt_filter.pack(side="left")
        
        ########################################################################################
        
        self.worker.start()
        self.after(1, self.check_queue)
        
        # self.pop_0()
        # self.pop_1()
        
    def destroy(self):
        mydict = dict(time=self.t_mismatches,m=self.mismatches)
        with open('dump.csv', 'w') as csv_file:
            writer = csv.writer(csv_file)
            keys = list(mydict.keys())
            writer.writerow(keys)
            for row in zip(*mydict.values()):
               writer.writerow(row)
        tk.Frame.destroy(self)
        
    def RS1_event(self, eclick, erelease):
        'eclick and erelease are the press and release events'
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        self.zeroth.x = [x1,x2]
        self.zeroth.y = [y1,y2]
        # def update_coords_entry():
            # e_02.delete(0,Tk.END)
            # e_02.insert(0,'x=[%3.2f,%3.2f],y=[%3.2f,%3.2f]' % (x1,x2,y1,y2))           
        # self.on_main_thread(update_coords_entry)
        
        self.graph1.ax1.RS1.set_active(False)
        self.graph1.ax1.RS1.set_visible(True)
        
    def RS2_event(self, eclick, erelease):
        'eclick and erelease are the press and release events'
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        self.second.x = [x1,x2]
        self.second.y = [y1,y2]
        # def update_coords_entry():
            # e_02.delete(0,Tk.END)
            # e_02.insert(0,'x=[%3.2f,%3.2f],y=[%3.2f,%3.2f]' % (x1,x2,y1,y2))           
        # self.on_main_thread(update_coords_entry)
        
        self.graph1.ax1.RS2.set_active(False)
        self.graph1.ax1.RS2.set_visible(True)
        
    def toggle_selector(self,event):
        print(' Key pressed.')
        if event.key in ['A', 'a'] and not self.graph1.ax1.RS1.active:
            print(' RectangleSelector activated.')
            self.graph1.ax1.RS1.set_active(True)
        if event.key in ['S', 's'] and self.graph1.ax1.RS1.active:
            print(' RectangleSelector deactivated.')
            self.graph1.ax1.RS1.set_active(False)
        if event.key in ['Z', 'z'] and not self.graph1.ax1.RS2.active:
            print(' RectangleSelector activated.')
            self.graph1.ax1.RS2.set_active(True)
        if event.key in ['X', 'x'] and not self.graph1.ax1.RS2.active:
            print(' RectangleSelector activated.')
            self.graph1.ax1.RS2.set_active(False)
        if event.key in ['D', 'd']:
            self.graph1.ax1.RS1.set_active(False)
            self.graph1.ax1.RS2.set_active(False)
            self.graph1.ax1.RS1.set_visible(False)
            self.graph1.ax1.RS2.set_visible(False)
            self.zeroth.x = [0,0]
            self.zeroth.y = [0,0]
            self.second.x = [0,0]
            self.second.y = [0,0]
        
    def pop_0(self):
        try:
            if self.win0.winfo_exists():
                self.win0.destroy()
        except AttributeError:
            pass
        self.win0 = tk.Toplevel(master=self)
        canvas0 = FigureCanvasTkAgg(self.graph0,master=self.win0).get_tk_widget()
        canvas0.pack(side="top",fill="both", expand=True)
        
    def pop_1(self):
        try:
            if self.win1.winfo_exists():
                self.win1.destroy()
        except AttributeError:
            pass
        self.win1 = PlotWindow(master=self)
        
        self.canvas1 = FigureCanvasTkAgg(self.graph1,master=self.win1).get_tk_widget()
        self.canvas1.pack(side="top",fill="both", expand=True)
        
        def onselect(RS, click, event):
            pass
        
        rectprops1 = dict(facecolor='red', edgecolor = 'black', alpha=0.2, fill=True)
        self.graph1.ax1.RS1 = RectangleSelector(self.graph1.ax1, self.RS1_event, button=[1, 3],rectprops=rectprops1)
        self.graph1.ax1.RS1.set_active(False)
        
        rectprops2 = dict(facecolor='blue', edgecolor = 'black', alpha=0.2, fill=True)
        self.graph1.ax1.RS2 = RectangleSelector(self.graph1.ax1, self.RS2_event,button=[1, 3],rectprops=rectprops2)
        self.graph1.ax1.RS2.set_active(False)
        
        self.graph1.canvas.mpl_connect('key_press_event', self.toggle_selector)        
        
    def check_queue(self):
        while True:
            try:
                task = self.queue.get(block=False)
                # print('got task ', task)
                # if q is full it will raise queue.Empty
            except queue.Empty:
                # print('empty queue')
                break
            else:
                # print('running task')
                # execute task() when the gui is idle
                self.after_idle(task)
        # print('scheduling next queue check')
        self.after(10, self.check_queue)
        
    def crop_by_pzt(self,dc,pzt_lims):            
        mask = (dc['pzt'] > pzt_lims[0]) & (dc['pzt'] < pzt_lims[1])         
        dc2 = {}
        for key in dc:
            dc2[key] = dc[key][mask]        
        return dc2
        
    def kill_by_grad(self,t,pzt):
        kill_time = self.kill_time
        dt = t[1]-t[0] # seconds/samples
        kill_samples = int(np.round(kill_time/dt)) # samples
        # kill_samples = 10000
        pzt_grad = np.gradient(pzt)/dt
        good_ind = []
        
        if kill_samples == 0:
            # all indices are good
            good_ind = list(range(len(pzt)))
        else:
            # build a list of good ind by jumping by kill_samples if gradient is too negative
            jj = 0
            for i in range(len(pzt)):
                if jj > len(pzt)-1:
                    break
                # print(jj)
                grad = pzt_grad[jj]
                if grad > -1*self.pzt_lim[1]:
                    good_ind.append(jj)
                    jj += 1
                else:
                    jj += kill_samples
        
        return good_ind
        
    def pull_channels_loop(self):
        t_start = self.omc_chan.t1
        while True:
                
            duration = self.duration
            # grab one second before current time because the current
            # second might not have been uploaded by nds yet
            offset = 1
            # t_start = int(t_now) - duration - offset
            
            omc = self.omc_chan.getdata(duration) - self.dark_level
            pzt = self.pzt_chan.getdata(duration)
            
            t_end = t_start + duration
            t_vec = np.linspace(t_start,t_end,len(omc))
            t_start = t_end
            
            # print(len(omc),len(pzt))
            
            mydecimate = lambda x,q: x[::q]
            
            t_vec = mydecimate(t_vec,self.q)
            omc = mydecimate(omc,self.q)
            pzt = mydecimate(pzt,self.q)
            
            # print('at grad kill')
            # print(len(t_vec),len(pzt))
            
            if self.rb_pzt_filter.var.get() == 'filter':
                good_ind = self.kill_by_grad(t_vec,pzt)
                
                t_vec = t_vec[good_ind]
                omc = omc[good_ind]
                pzt = pzt[good_ind]
            
            if len(t_vec) < 2:
                # if the block is less than 2 samples then just skip the entire block
                continue
            
            dt = t_vec[1]- t_vec[0]
            
            # print('at filt', len(omc),len(pzt))

            sort_ind = np.argsort(pzt)
            pzt_sort = pzt[sort_ind]
            omc_sort = omc[sort_ind]
            
            # print('at lowpass')
            
            if self.lowpass:
                omc_filt = brick_lowpass(omc,10,M=50,wn_fun=hann_wn)
            else:
                omc_filt = omc
            omc_filt_sort = omc_filt[sort_ind]

            # data needs to be packaged this way for OMC_scan lib
            dc = {}
            dc['omc'] = omc_filt_sort
            dc['pzt'] = pzt_sort
                
            dc00 = self.crop_by_pzt(dc,self.zeroth.x)
            dc02 = self.crop_by_pzt(dc,self.second.x)
            
            if (len(dc00['omc']) < 2) or (len(dc02['omc']) < 2):
                # at least one of the selection boxes is empty
                # turn off ratio measurement
                self.valid_boxes = False
            else:
                # boxes contain valid data
                # take ratio of maxes in each box
                # user is responsible for checking that the max in the box corresponds to the peak
                self.valid_boxes = True
                               
                pidx_00 = np.argmax(dc00['omc'])
                pidx_02 = np.argmax(dc02['omc'])
                
                a_00 = dc00['omc'][pidx_00]
                a_02 = dc02['omc'][pidx_02]
                
                self.mismatches.append(a_02/a_00)
                self.t_mismatches.append(t_end)
                
            if self.rb_yscale.var.get() == 'semilogy':
                self.graph0.ax2.set_yscale('log')
                self.graph1.ax1.set_yscale('log')
            else:
                self.graph0.ax2.set_yscale('linear')
                self.graph1.ax1.set_yscale('linear')
                
            self.graph0.line1.set_data(t_vec,pzt)
            self.graph0.line1.set_linestyle('')
            self.graph0.line1.set_marker('.')
            self.graph0.line1.set_ms(1)
            self.graph0.ax1.set_xlim(min(t_vec),max(t_vec))
            self.graph0.ax1.set_ylim(self.pzt_lim)
            
            self.graph0.line2.set_data(t_vec,omc)
            self.graph0.line2.set_linestyle('')
            self.graph0.line2.set_marker('.')
            self.graph0.line2.set_ms(1)            
            # self.graph0.line3.set_color('red')
            self.graph0.ax2.set_xlim(min(t_vec),max(t_vec))
            self.graph0.ax2.set_ylim(self.omc_lim)
            
            self.graph0.canvas.draw()
            
            self.graph1.line1.set_data(dc['pzt'],dc['omc'])
            self.graph1.ax1.set_xlim(self.pzt_lim)
            self.graph1.ax1.set_ylim(self.omc_lim)
            self.graph1.ax1.set_ylim(self.omc_lim)
            
            self.graph1.line2.set_data(self.t_mismatches[-self.m_range:],self.mismatches[-self.m_range:])
            self.graph1.ax2.relim()
            self.graph1.ax2.autoscale()
            
            self.graph1.canvas.draw()
