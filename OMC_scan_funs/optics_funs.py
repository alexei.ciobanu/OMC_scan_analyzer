import numpy as np
import pandas as pd
import scipy.integrate as intgr

def grin_abcd(d,n0,n2):
    # siegman p586 "h"
    return np.array(\
                    [[np.cos(d*np.sqrt(n2/n0)),np.sin(d*np.sqrt(n2/n0))/(np.sqrt(n2*n0))],\
                     [-np.sqrt(n2*n0)*np.sin(d*np.sqrt(n2/n0)),np.cos(d*np.sqrt(n2/n0))]]\
                   )

def space_abcd(d):
    return np.array(\
                   [[1,d],\
                    [0,1]]\
                   )

def q_propag(q,M):
    A,B,C,D = M.flatten()
    return (A*q + B)/(C*q + D)
    
def herm(n,x):
    from numpy.polynomial import hermite
    
    c = np.zeros(n+1)
    c[-1] = 1
    return hermite.hermval(x,c)

def u_n(n,x,z,w0,lam=1064e-9):
    import numpy as np
    
    zR = np.pi*w0**2/lam
    k = 2*np.pi/lam
    w = w0*np.sqrt(1+(z/zR)**2)
    
    if z == 0:
        R = np.inf
    else:
        R = (z+np.spacing(0.0))*(1+(zR/z)**2)
    psi = np.arctan(z/zR)
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(np.exp(1j*(2*n+1)*psi)/(2**n*np.math.factorial(n)*w))
#     t2 = 1
    t3 = herm(n,np.sqrt(2)*x/w)
    a1 = 0 # -1j*k*z
    a2 = 1j*k*x**2/(2*R)
    a3 = x**2/w**2
    t4 = np.exp(a1 - a2 - a3)
    E = t1 * t2 * t3 * t4
    
    return E

def u_nm(n,m,x,y,z,w0,lam=1064e-9,gamma_x=0):
    return np.outer(u_n(m,y,z,w0,lam),u_n(n,x,z,w0,lam))
    
def u_n_q(n,x,q,lam=1064e-9):
    import numpy as np
    factorial = np.math.factorial
    
    q0 = q - np.real(q)
    w0 = np.sqrt(lam*np.imag(q0)/np.pi)
    w = np.sqrt(-lam/(np.pi*np.imag(1/q)))
    k = 2*np.pi/lam
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(1/(2**n*factorial(n)*w0))
    t3 = np.sqrt(q0/q)
    t4 = (q0/np.conj(q0) * np.conj(q)/q)**(n/2)
    t5 = herm(n,np.sqrt(2)*x/w) * np.exp(-1j*k*x**2/(2*q))
    
    E = t1 * t2 * t3 * t4 * t5
    
    return E
    
def u_nm_q(x,y,q,n=0,m=0,lam=1064e-9):
    return np.outer(u_n_q(m,y,q,lam),u_n_q(n,x,q,lam))
    
def u_nm_q_astig(x,y,qx,qy,n=0,m=0,lam=1064e-9):
    return np.outer(u_n_q(m,y,qy,lam),u_n_q(n,x,qx,lam))
    
def q_create(z,w0,lam):
    return z + 1j*np.pi*w0**2/lam
    
def q2guoy(q,n=0,m=0):
    z = np.real(q)
    zR = np.imag(q)
    psi = np.arctan(z/zR)
    return np.sqrt(np.exp(1j*(2*(n+1))*psi)) * np.sqrt(np.exp(1j*(2*(m+1))*psi))
    
def q2w(q,lam=1064e-9):
    import numpy as np
    return np.sqrt(-lam/(np.pi*np.imag(1/q)))

def q2w0(q,lam=1064e-9):
    import numpy as np
    q0 = q - np.real(q)
    return np.sqrt(-lam/(np.pi*np.imag(1/q0)))

def q2Theta(q,lam=1064e-9):
    w0 = q2w0(q,lam)
    return lam/(np.pi*w0)

def DHT(F, x, y, z, w0, lam=1064e-9, maxorder=10, type='Riemann'):
    N = maxorder+1
    H = np.zeros([N,N],dtype=np.complex128)
    dx = abs(x[1] - x[0])
    dy = abs(y[1] - y[0])

    for ii in range(N):
        for jj in range(N):
            herm = u_nm(ii,jj,x,y,z,w0,lam)
            if type == 'Simpson':
                H[ii,jj] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
            elif type == 'Riemann':
                H[ii,jj] = np.sum((F*np.conj(herm)))*dx*dy
            
    return H
    
def DHT_q(F, x, y, q, lam=1064e-9, maxorder=10, type='Riemann'):
    N = maxorder+1
    H = np.zeros([N,N],dtype=np.complex128)
    dx = abs(x[1] - x[0])
    dy = abs(y[1] - y[0])
    
    for ii in range(N):
        for jj in range(N):
            herm = u_nm_q(x,y,q,ii,jj,lam)
            if type == 'Simpson':
                H[ii,jj] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
            elif type == 'Riemann':
                H[ii,jj] = np.sum((F*np.conj(herm)))*dx*dy
            
    return H
    
def BH_DHT(q1,q2,n1=0,m1=0,dx=0,dy=0,gammax=0,gammay=0,maxtem=10):
    # Bayer-Helms DHT (mostly for debugging Bayer-Helms)
    N = maxtem+1
    H = np.zeros([N,N],dtype=np.complex128)   

    for ii in range(N):
        for jj in range(N):
            H[jj,ii] = k_fun(n1,jj,q1,q2,dx=dx,gamma=gammax)*k_fun(m1,ii,q1,q2,dx=dy,gamma=gammay)
            
    return H 
    
def BH_DHT_astig(q1x,q1y,q2x,q2y,n1=0,m1=0,dx=0,dy=0,gammax=0,gammay=0,maxtem=10):
    # Bayer-Helms DHT (mostly for debugging Bayer-Helms)
    N = maxtem+1
    H = np.zeros([N,N],dtype=np.complex128)   

    for ii in range(N):
        for jj in range(N):
            H[jj,ii] = k_fun(n1,jj,q1x,q2y,dx=dx,gamma=gammax)*k_fun(m1,ii,q1y,q2x,dx=dy,gamma=gammay)
            
    return H  

def iDHT(H, x, y, z, w0, lam):
    N = len(H)
    F = 0
    for m in range(N):
        for n in range(N):
            F = F + H[m,n]*u_nm(n,m,x,y,z,w0,lam)
    return F

def parse_DHT(H,power=False,sortby='magnitude'):
    out_list = []
    for n in range(len(H)):
        for m in range(len(H)):
            out_list.append([abs(H[n,m]),np.angle(H[n,m],deg=True),n,m])
    df = pd.DataFrame(out_list)
    df.columns = ['abs','deg','n','m']
    if sortby == 'magnitude':
        df = df.sort_values('abs',ascending=False).reset_index(drop=True)
    elif sortby == 'n,m':
        df = df.sort_values(by=['m','n'],ascending=True).reset_index(drop=True)
    if power:
        df['abs'] = df['abs']**2
        df.columns = ['power','deg','n','m']
    return df
    
def maxtem_scan(F, x, y, z, w0, lam, maxorder):
    iDHT_s = lambda H: iDHT(H, x, y, z, w0, lam)
    DHT_s = lambda F, maxtem: DHT(F,x,y,z,w0,lam,maxtem)
    dx = np.abs(x[0] - x[1])
    dy = np.abs(y[0] - y[1])
    maxtems = np.arange(0,maxorder+1,1)
    out_list = []
    for maxtem in maxtems:
        residual = iDHT_s(DHT_s(F,maxtem)) - F
        res_int = np.sum(np.abs(residual)**2)*dx*dy
        out_list.append([maxtem,res_int])
    return out_list
    
def radial_zernike(n,m,r):
    
    r = np.asarray(r)
    scalar_input = False
    if r.ndim == 0:
        r = r[None]  # Makes x 1D
        scalar_input = True

    # The magic happens here
    
#     r[r>1] = np.nan
    
    fac = np.math.factorial
    t = 0
    if np.mod(n-m,2) == 0:
        for k in range((n-m)//2+1):
            num = ((-1)**k * fac(n-k))
            denom = (fac(k)*fac((n+m)/2-k)*fac((n-m)/2-k))
            t += num/denom * r**(n-2*k)  
        return t
        if scalar_input:
            return np.squeeze(t)
        else: return t
    else:
        return np.zeros(np.shape(r))
    
def zernike(n,m,r,phi):

    if m >= 0:
#         return np.einsum('i,j->ij',radial_zernike(n,m,r),np.cos(m*phi))
        return np.multiply.outer(radial_zernike(n,m,r),np.cos(m*phi))
    else:
#         return np.einsum('i,j->ij',radial_zernike(n,-m,r),np.sin(m*phi))
        return np.multiply.outer(radial_zernike(n,-m,r),np.sin(m*phi))
        
def DZT(F, r, phi, maxorder):
    # Discrete Zernike Transform
    # n -> [0:N]
    # m -> [-N:N]
    N = maxorder+1
    H = np.zeros([N,N*2-1])
    dr = abs(r[1] - r[0])
    dphi = abs(phi[1] - phi[0])

    for n in range(N):
        for m in range(-n,n+1):
#             print(ii,jj,np.shape(r),np.shape(phi))
            if m == 0:
                epsilon_m = 2
            else:
                epsilon_m = 1            
            norm_fac = (2*n+2)/(np.pi*epsilon_m)
#             norm_fac = 1
            
            test_fun = zernike(n,m,r,phi)
            H[n,m] = intgr.simps(intgr.simps(F*np.conj(test_fun)*r[:,None]))*dr*dphi * norm_fac
            
    return H

def iDZT(Z,r,phi):
    N = np.shape(Z)[0]
    F = 0
    for n in range(N):
        for m in range(-n,n+1):
            F = F + Z[n,m]*zernike(n,m,r,phi)            
    return F
    
def parse_DZT(Z):
    N = np.shape(Z)[0]
    out_list = []
    for n in range(N):
        for m in range(-n,n+1):
            out_list.append([abs(Z[n,m]),n,m])
    df = pd.DataFrame(out_list)
    df.columns = ['abs','n','m']
    return df.sort_values('abs',ascending=False)
    
# numeric version of coupling coeffs
def k_fun(n1,n2,q1,q2,lam=1064e-9,gamma=0,dx=0):
    
    import numpy as np
    fac = np.math.factorial
    sqrt = np.lib.scimath.sqrt
    Rational = lambda x,y: x/y
    
    # rip out subcomponents of q
    z1 = np.real(q1)
    z2 = np.real(q2)
    zR1 = np.imag(q1)
    zR2 = np.imag(q2)
    w01 = sqrt(lam*zR1/np.pi)
    w02 = sqrt(lam*zR2/np.pi)
    
    # Bayer-Helms sub terms
    K2 = (z1-z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = 1j*np.conj(q1-q2)/(2*np.imag(q2))
    K = (K0 + 1j*K2)/2
    #X_bar = (1j*zR2 - z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - 1j)*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    X = (dx/w02-(z2/zR2 + 1j*(1+2*np.conj(K)))*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    F_bar = K/(2*(1+K0))
    F = np.conj(K)/2
    
    E_x = np.exp(-(X*X_bar)/2 - 1j*dx/w02 * np.sin(gamma)*zR2/w02)
    # note that there is a typo in BH paper for the E_x term where he uses w02 == bar(w0) 
    # in the 1j*dx/w0 term instead of w01 (ie w0 without a bar)
    # upon closer inspection it looks like BH agrees with Riemann if the last index matches 
    # that of the term multiplying it 
    
    # i.e. 
    # 1j*dx/w02 * np.sin(gamma)*zR2/w02 (choose this one arbitrarily)
    # or 
    # 1j*dx/w01 * np.sin(gamma)*zR1/w01
    # give identical results equivelant with Riemann 
    
    # whereas
    # 1j*dx/w02 * np.sin(gamma)*zR1/w01 (orginal BH)
    # or
    # 1j*dx/w01 * np.sin(gamma)*zR2/w02
    # do not
    
    def S_g(n1,n2):
        s1 = 0
        for mu1 in range(0, (n1//2 if n1 % 2 == 0 else (n1-1)//2) + 1):
            for mu2 in range(0, (n2//2 if n2 % 2 == 0 else (n2-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**(n1-2*mu1)*X**(n2-2*mu2))/(fac(n1-2*mu1)*fac(n2-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma * F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma)*fac(mu1-sigma)*fac(mu2-sigma))
    #                 print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1

    def S_u(n1,n2):
        s1 = 0
        for mu1 in range(0, ((n1-1)//2 if (n1-1) % 2 == 0 else ((n1-1)-1)//2) + 1):
            for mu2 in range(0, ((n2-1)//2 if (n2-1) % 2 == 0 else ((n2-1)-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**((n1-1)-2*mu1)*X**((n2-1)-2*mu2))/(fac((n1-1)-2*mu1)*fac((n2-1)-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma*F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma+1)*fac(mu1-sigma)*fac(mu2-sigma))
#                     print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1
    
    expr = (-1)**n2 * E_x * sqrt(fac(n1)*fac(n2) \
    * (1 + K0)**(n1 + Rational(1,2)) * (1+np.conj(K))**(-(n1+n2+1))) \
    * (S_g(n1,n2) - S_u(n1,n2))
    
    return expr

def k_nmnm(n1,m1,n2,m2,q1,q2,gammax=0,gammay=0,dx=0,dy=0):
    return k_fun(n1,n2,q1,q2,gamma=gammax,dx=dx) * k_fun(m1,m2,q1,q2,gamma=gammay,dx=dy)
    
def mode_mismatch(q1,q2):
    return (np.abs(q2-q1)**2)/(np.abs(q2-np.conj(q1))**2)
    
def multigauss_overlap(q1,q2):
    return 1/2 + 1/4*k_nmnm(0,0,0,0,q1,q2) + 1/4*k_nmnm(0,0,0,0,q2,q1)
    
def multigauss_mismatch(q1,q2):
    # fractional power loss from adding two gaussian beams together
    return 1/2 - 1/4*k_nmnm(0,0,0,0,q1,q2) - 1/4*k_nmnm(0,0,0,0,q2,q1)
    
def mode_list(maxtem,even_only=False):
    modes_list = []
    for n in range(0,maxtem+1):
        for m in range(0,maxtem+1):
            if n+m <= maxtem:
                if even_only:
                    if np.mod(n,2)==0 and np.mod(m,2)==0 :
                        modes_list.append([n,m])
                else:
                    modes_list.append([n,m])
    return modes_list