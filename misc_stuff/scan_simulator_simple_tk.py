# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

#import matplotlib.pyplot as plt
import scipy.signal
import numpy as np
from subprocess import check_output
import matplotlib
matplotlib.use("TkAgg") # needs to be there
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.widgets import RadioButtons
from peak_finder import peakdet
from OMC_scan_funs_no_pandas_v2 import hamming_wn, brick_lowpass, hann_wn

import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk
    from tkinter import W

omc_channel = np.load('./omc.npy')
pzt_channel = np.load('./pzt.npy')

t1 = 1208580738
t2 = 1208582718

Fs = 2**14

time_channel = np.linspace(t1,t2,len(pzt_channel))
time_channel2 = np.linspace(t1,t2,len(omc_channel))

# pzt channel is downampled, reinterpolate
pzt_channel2 = np.interp(time_channel2,time_channel,pzt_channel)

print(len(pzt_channel2),len(omc_channel))

class channel:
    
    def __init__(self,stream):
        self.data = stream
        self.time = t1
        self.s1 = 200*Fs
    
    def getdata(self,duration,t_pull=1,t_start=None):
        self.s2 = self.s1 + Fs*duration
        self.time += duration
        grab = self.data[self.s1:self.s2]
        self.s1 += t_pull*Fs
        return grab

FSR=2.649748e+08

class globals_class:
    def __init__(self):
        self.OMC_DC_offset = -0.1
        self.s1 = 200*Fs

def lets_go():
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(411)
    ax2 = fig1.add_subplot(412)
    ax3 = fig1.add_subplot(413)
    ax4 = fig1.add_subplot(414)
    plt.show(block=False)
    
    omc_chan = channel(omc_channel)
    pzt_chan = channel(pzt_channel2)
    
    globals = globals_class()
    globals.duration = 12
    globals.q = 8
    globals.pzt_lim = [-10,120]
    globals.omc_lim = [7e-2,0.5]
    globals.OMC_DC_offset = -0.1
    globals.yscale = 'linear'
    globals.lowpass = False
    globals.pzt_filter = True
    globals.kill_time = 1
    
    globals.zeroth = globals_class()
    globals.zeroth.x = globals.pzt_lim
    globals.zeroth.y = globals.omc_lim
    
    todict = lambda **kwargs: kwargs
    globals.todict = todict
    def submit():
        text = e.get()
        in_dict = eval('globals.todict('+text+')')
        for key in in_dict:
            setattr(globals, key, in_dict[key])
            
    def set_yscale():
        label = str(yaxis_var.get())
        globals.yscale=label
        
    def set_lowpass():
        label = str(lowpass_var.get())
        if label == 'lowpass':
            globals.lowpass=True
        else:
            globals.lowpass=False
           
    def set_pzt_filter():
        label = str(pzt_var.get())
        if label == 'filter':
            globals.pzt_filter=True
        else:
            globals.pzt_filter=False
            
    def aquire_00():
        text = str(e_00.get())
        in_dict = eval('globals.todict('+text+')') 
        for key in in_dict:
            setattr(globals.zeroth, key, in_dict[key])
        return
    
    root = Tk.Tk()
    root.grid()
    
    r = 0
    
    fig2 = plt.Figure()
    fig2.set_size_inches(3,2)
    ax2_0 = fig2.add_subplot(111)

    plot2 = Tk.Frame(root)
    plot2.grid(row=r,column=2)
    canvas = FigureCanvasTkAgg(fig2, master=plot2)  # A tk.DrawingArea.
    canvas.get_tk_widget().pack( side = Tk.RIGHT, expand=1)
    canvas.draw()
    
    e_00 = Tk.Entry(master=root,width=26)
    e_00.grid(row=r,column=0)
    e_00.insert(0,'x=[85,90],y={0}'.format(globals.omc_lim))
    
    b_00 = Tk.Button(master=root, text='aquire', command=aquire_00).grid(row=r,column=1)
    
    r+=1
    
    pzt_var = Tk.StringVar(master=root)
    pzt_var.set('filter') # select default option
    set_pzt_filter()

    rb_pzt1 = Tk.Radiobutton(master=root, text="filter", variable=pzt_var, value='filter',command=set_pzt_filter).grid(row=r,column=0)
    rb_pzt2 = Tk.Radiobutton(master=root, text="no filter", variable=pzt_var, value='no filter',command=set_pzt_filter).grid(row=r,column=1)
    e_pzt = Tk.Entry(master=root)
    e_pzt.grid(row=r,column=2)
    e_pzt.insert(0, 'NA')
    
    r+=1
    
    yaxis_var = Tk.StringVar(master=root)
    yaxis_var.set('linear') # select default option
    set_yscale()

    rb_yax1 = Tk.Radiobutton(master=root, text="semilogy", variable=yaxis_var, value='semilogy',command=set_yscale).grid(row=r,column=0)
    rb_yax2 = Tk.Radiobutton(master=root, text="linear", variable=yaxis_var, value='linear',command=set_yscale).grid(row=r,column=1)
    
    r+=1
    
    lowpass_var = Tk.StringVar(master=root)
    lowpass_var.set('no lowpass') # select default option
    set_lowpass()

    rb_low1 = Tk.Radiobutton(master=root, text="lowpass", variable=lowpass_var, value='lowpass',command=set_lowpass).grid(row=r,column=0)
    rb_low2 = Tk.Radiobutton(master=root, text="no lowpass", variable=lowpass_var, value='no lowpass',command=set_lowpass).grid(row=r,column=1)
    
    r+=1
    
    initial_text = 'duration={0},pzt_lim={1},omc_lim={2},q={3},kill_time={4}'\
    .format(globals.duration,globals.pzt_lim,globals.omc_lim,globals.q,globals.kill_time)
    
    e = Tk.Entry(master=root,width=1000)
    e.grid(row=r,column=0,columnspan=4)
    e.insert(0, initial_text)
    
    r+=1
    
    quit_button = Tk.Button(master=root, text='Kill', command=root.destroy).grid(row=r,column=0)
    submit_button = Tk.Button(master=root, text='Submit', command=submit).grid(row=r,column=1)
    
    n_rows = r
    n_cols = 5
    for x in range(n_cols):
        root.grid_columnconfigure(x,weight=1)
    
    for x in range(n_rows):
        root.grid_rowconfigure(x,weight=1)
    
    root.geometry('{}x{}'.format(640, 480))
    
    def kill_by_grad(t,pzt):
        kill_time = globals.kill_time
        dt = t[1]-t[0] # seconds/samples
        kill_samples = int(np.round(kill_time/dt)) # samples
        # kill_samples = 10000
        pzt_grad = np.gradient(pzt)/dt
        good_ind = []
        
        if kill_samples == 0:
            # all indices are good
            good_ind = list(range(len(pzt)))
        else:
            # build a list of good ind by jumping by kill_samples if gradient is too negative
            jj = 0
            for i in range(len(pzt)):
                if jj > len(pzt)-1:
                    break
                # print(jj)
                grad = pzt_grad[jj]
                if grad > -1*globals.pzt_lim[1]:
                    good_ind.append(jj)
                    jj += 1
                else:
                    jj += kill_samples
        
        return good_ind
    
    ts = []
    mismatches = []
    t_start = t1 + 120
    
    # in Tk looping works weirdly
    # the mainloop() will block further execution, so you need to call after(t,fun)
    # before mainloop() which will execute fun t ms after mainloop()
    # then before you return from fun you call after(t,fun) again and create
    # perpetual loop of renewed after(t,fun). Very weird.
    def ayy(t_start):
        duration = globals.duration
        # grab one second before current time because the current
        # second might not have been uploaded by nds yet
        offset = 1
        # t_start = int(t_now) - duration - offset
            
        omc = omc_chan.getdata(duration) - globals.OMC_DC_offset
        pzt = pzt_chan.getdata(duration)
        
        t_end = t_start + duration
        t_vec = np.linspace(t_start,t_end,len(omc))
        t_start = t_end
        
        mydecimate = lambda x,q: x[::q]
        
        t_vec = mydecimate(t_vec,globals.q)
        omc = mydecimate(omc,globals.q)
        pzt = mydecimate(pzt,globals.q)
        
        print('at grad kill')
        if globals.pzt_filter:
            good_ind = kill_by_grad(t_vec,pzt)
            
            t_vec = t_vec[good_ind]
            omc = omc[good_ind]
            pzt = pzt[good_ind]
        
        if len(t_vec) < 2:
            plt.pause(0.01)
            globals.root.after(1,ayy,t_start)
            return
        
        dt = t_vec[1]- t_vec[0]
        
        print(len(omc),len(pzt))

        sort_ind = np.argsort(pzt)
        pzt_sort = pzt[sort_ind]
        omc_sort = omc[sort_ind]
        
        if globals.lowpass:
            omc_filt = brick_lowpass(omc,10,M=50,wn_fun=hann_wn)
        else:
            omc_filt = omc
        omc_filt_sort = omc_filt[sort_ind]

        # data needs to be packaged this way for OMC_scan lib
        dc = {}
        dc['omc'] = omc_filt_sort
        dc['pzt'] = pzt_sort
        

        #TODO: define better deltas
        # delta_upper = np.max(dc['omc'])/1.3
        # delta_lower = np.max(dc['omc'])/10
        #delta = 1e-4 # chosen for noise plotting
        
        peak_ind = []
        # peak_ind = peakdet(omc_filt_sort,delta_upper)
        
        # print('at plot')

        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()
        
        ax1.plot(t_vec,pzt,'.')
        ax1.set_ylim(globals.pzt_lim)
        
        ax2_0.clear()
        ax2_0.plot(dc['pzt'],dc['omc'])
        ax2_0.set_xlim(globals.zeroth.x)
        ax2_0.set_ylim(globals.zeroth.y)

        if globals.yscale == 'semilogy':
            ax2.semilogy(t_vec,omc)
            if globals.lowpass:
                ax2.semilogy(t_vec,omc_filt,c='r')
            ax3.semilogy(dc['pzt'],dc['omc'])
            ax3.semilogy(dc['pzt'][peak_ind],dc['omc'][peak_ind],'x',c='r')
        else:
            ax2.plot(t_vec,omc,'.',ms=0.5)
            if globals.lowpass:
                ax2.plot(t_vec,omc_filt,c='r')
            ax3.plot(dc['pzt'],dc['omc'])
            ax3.plot(dc['pzt'][peak_ind],dc['omc'][peak_ind],'x',c='r')
            
        ax2.set_ylim(globals.omc_lim)
        ax3.set_xlim(globals.pzt_lim)
        ax3.set_ylim(globals.omc_lim)

        fig1.canvas.draw() # it's like plt.draw() + sleep(0.1)
        fig2.canvas.draw()
        # plt.pause(0.01)
        
        # reapply after() to make Tk update the plot
        globals.root.after(1,ayy,t_start)
        
        return
    
    # stor the Tk app in globals so that the plotting code can call after() 
    globals.root = root
    
    # after() is what allows Tk apps to execute other code
    root.after(1,ayy, t_start)
    root.mainloop()

    return