# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

import time
import threading
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.widgets import  RectangleSelector
import numpy as np
import numpy.random
import time

# import sys
# if sys.version_info[0] < 3:
    # import Tkinter as Tk
    # import Queue as queue
# else:
    # import tkinter as Tk
    # import queue

def ayy():
            
    def onselect(eclick, erelease):
        'eclick and erelease are matplotlib events at press and release'
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        print(' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata))
        print(' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata))
        print(' used button   : ', eclick.button)
        toggle_selector.RS.set_active(False)

    def toggle_selector(event):
        print(' Key pressed.')
        if event.key in ['Q', 'q'] and toggle_selector.RS.active:
            print(' RectangleSelector deactivated.')
            toggle_selector.RS.set_active(False)
        if event.key in ['A', 'a'] and not toggle_selector.RS.active:
            print(' RectangleSelector activated.')
            toggle_selector.RS.set_active(True)

    class MyRectangleSelector(RectangleSelector):
        def release(self, event):
            super(MyRectangleSelector, self).release(event)
            self.to_draw.set_visible(True)
            self.canvas.draw()
            
    fig0 = plt.figure()
    
    ax0_1 = fig0.add_subplot(211)
    ax0_2 = fig0.add_subplot(212)
    
    line0_1, = ax0_1.plot([],[])
    line0_2, = ax0_2.plot([],[])
    
    fig1 = plt.figure()
    
    # remove all default mpl keybindings for fig1
    fig1.canvas.mpl_disconnect(fig1.canvas.manager.key_press_handler_id)
    
    ax1_1 = fig1.add_subplot(211)
    ax1_2 = fig1.add_subplot(212)
    
    line1_1, = ax1_1.plot([],[])
    line1_2, = ax1_2.plot([],[])
         
    toggle_selector.RS = MyRectangleSelector(ax1_1, onselect,
                                   drawtype='box', useblit=True,
                                   button=[1, 3],  # don't use middle button
                                   minspanx=5, minspany=5,
                                   spancoords='pixels')
                                   
    toggle_selector.RS.set_active(False)
    fig1.canvas.mpl_connect('key_press_event', toggle_selector)

    ax0_1.set_xlim([0,100])
    ax0_1.set_ylim([0,1])
    ax0_2.set_xlim([0,100])
    ax0_2.set_ylim([0,1])
    ax1_1.set_xlim([0,100])
    ax1_1.set_ylim([0,1])
    while plt.fignum_exists(1):
        
        x = np.arange(100)
        y = np.random.rand(100)

        line0_1.set_data(x,np.random.rand(100))
        line0_2.set_data(x,np.random.rand(100))
        
        line1_1.set_data(x,np.random.rand(100))
        
        plt.pause(0.1)

# print(future.result())

# after() is what allows Tk apps to execute other code
# root.after(1, root.check_queue)
t = threading.Thread(target = ayy)
t.start()

j = 0
while True:
    j+= 1
    time.sleep(1)
    print(j)
# future = executor.submit(callback, t1)
# print(future.exception())

# root.mainloop()
# print('killed')
# print(future.cancel())
print(globals.alive)