# plots omc dcpd vs time in a single window

import cdsutils
import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output

channel_name = 'H1:ASC-AS_B_DC_NSUM_OUT16'
channel_name = 'H1:OMC-DCPD_SUM_OUT_DQ'

def lets_go():
	fig = plt.figure()
	plt.show(block=False)
	while True:
		t_now = check_output(['tconvert', 'now'])
		duration = 10 # seconds
		t_start = int(t_now) - duration
		data_obj = cdsutils.getdata(channel_name,duration,t_start)
		if data_obj is None:
			print('empty data_obj returned')
			continue
		ydata = data_obj.data
		xdata = np.linspace(t_start,t_start+10,len(ydata))
		fig.clear()
		plt.plot(xdata,ydata)
		plt.pause(0.5)
	return

	
