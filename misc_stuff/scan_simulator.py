# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

#import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output
import matplotlib
matplotlib.use("TkAgg") # needs to be there
import matplotlib.pyplot as plt
from OMC_scan_funs_no_pandas_v2 import crop_data, stage2, stage3, hamming_wn, apply_lowpass_omc, bootstrap_failed, brick_lowpass, hann_wn

omc_channel = np.load('./omc.npy')
pzt_channel = np.load('./pzt.npy')

t1 = 1208580738
t2 = 1208582718

Fs = 2**14

time_channel = np.linspace(t1,t2,len(pzt_channel))
time_channel2 = np.linspace(t1,t2,len(omc_channel))

# pzt channel is downampled, reinterpolate
pzt_channel2 = np.interp(time_channel2,time_channel,pzt_channel)

class channel:
    
    def __init__(self,stream):
        self.data = stream
        self.time = t1
        self.s1 = 0
    
    def getdata(self,duration,t_start=None):
        self.s2 = self.s1 + Fs*duration
        grab = self.data[self.s1:self.s2]
        self.s1 = self.s2
        return grab
        
    def getdata_pzt(self,duration,t_start):
        self.s2 = self.s1 + Fs*duration
        grab = self.pzt[s1:s2]
        self.s1 = self.s2
        return grab

FSR=2.649748e+08

def lets_go():
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(411)
    ax2 = fig1.add_subplot(412)
    ax3 = fig1.add_subplot(413)
    ax4 = fig1.add_subplot(414)
    plt.show(block=False)
    
    OMC_DC_offset = 0

    ts = []
    mismatches = []
    while True:
        duration = 10
        # grab one second before current time because the current
        # second might not have been uploaded by nds yet
        offset = 1
        # t_start = int(t_now) - duration - offset
        
        omc_chan = channel(omc_channel)
        pzt_chan = channel(pzt_channel)
            
        omc = omc_chan.getdata(duration) - OMC_DC_offset
        pzt = pzt_chan.getdata(duration)

        sort_ind = np.argsort(pzt)
        pzt_sort = pzt[sort_ind]
        omc_sort = omc[sort_ind]
        
        print('at_filt ',len(omc),len(pzt))
        
        omc_filt = brick_lowpass(omc,50,M=75,wn_fun=hann_wn)
        omc_filt_sort = omc_filt[sort_ind]

        # data needs to be packaged this way for OMC_scan lib
        dc = {}
        dc['omc'] = omc_filt_sort
        dc['pzt'] = pzt_sort
        
        # if np.max(np.abs(dc['pzt'])) > 50:
            # print('ay')
            # dc = crop_data(dc,keep_range=[0.05,1])
            # #need to round to nearest power of 2
            # N = len(dc['pzt'])
            # N_new = int(2**(np.floor(np.log2(N))))
            # dc = digitize_output(dc,target='pzt',N_bins=N_new,verbose=False)

        #TODO: define better deltas
        delta_upper = np.max(dc['omc'])/1.3
        delta_lower = np.max(dc['omc'])/10
        #delta = 1e-4 # chosen for noise plotting
        
        print('at try')
        try:
            #TODO: allow stage2 to fail (ie return None)
            dc2 = stage2(dc,delta_lower=delta_lower,delta_upper=delta_upper,use_matched_filt=True)
            
            #TODO: make stage3 return peak locs/labels (useful for visually tracking algo errors)
            #mismatch = stage3(dc2,delta = delta_lower,verbose=False)
            
            
            #TODO: write out these two in append mode to some file
            #ts.append(t_now)
            #mismatches.append(mismatch)
        except bootstrap_failed:
            # thats fine. just try again next iteration
            dc2 = {}
            dc2['freq'] = []
            dc2['omc'] = []
            
        print('at plot')
        xdata = np.linspace(t_start,t_start+10,len(omc))

        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()
        ax1.plot(xdata,pzt)
        ax2.plot(xdata,omc)
        ax2.plot(xdata,omc_filt,c='r')
        ax3.plot(dc['pzt'],dc['omc'])
        #TODO: plot peak locs here
        # ax3.plot(pzt_sort[peak_ind],omc_sort[peak_ind],'x',c='r',ms=6,mew=2)		
        ax4.plot(ts,mismatches)
        plt.pause(0.1) # it's like plt.draw() + sleep(0.1)
    return
