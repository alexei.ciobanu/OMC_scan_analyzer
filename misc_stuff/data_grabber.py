import cdsutils
import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output

omc_out = 'H1:OMC-DCPD_SUM_OUT_DQ'
pzt_out_DC_OUT_DQ = 'H1:OMC-PZT2_MON_DC_OUT_DQ'

t1 = 1208580738
t2 = 1208582718
dt = t2 - t1

pzt_obj = cdsutils.getdata(pzt_out_DC_OUT_DQ,dt,t1)
omc_obj = cdsutils.getdata(omc_out,dt,t1)

np.save('omc',omc_obj.data)
np.save('pzt',pzt_obj.data)

omc_time = np.linspace(t1,t2,len(omc_obj.data))
pzt_time = np.linspace(t1,t2,len(pzt_obj.data))

	
