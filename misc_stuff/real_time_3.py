# plots omc dcpd vs time 
# and pzt vs time
# and omc vs pzt in a single window

import cdsutils
import matplotlib.pyplot as plt
import time
import numpy as np
from subprocess import check_output

channel_name = 'H1:ASC-AS_B_DC_NSUM_OUT16'
omc_channel_name = 'H1:OMC-DCPD_SUM_OUT_DQ'
pzt_channel_name = 'H1:OMC-PZT2_EXC'

def lorentzian(fwhm,x):
    return 0.5/np.pi*fwhm/(x**2+(0.5*fwhm)**2)

def lets_go():
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(311)
	ax2 = fig1.add_subplot(312)
	ax3 = fig1.add_subplot(313)
	plt.show(block=False)
	while True:
		t_now = check_output(['tconvert', 'now'])
		# carefully chosen as pzt excitation period to avoid 
		# multiple omc vals at a single pzt val
		duration = 10 
		# grab one second before current time because the current
		# second might not have been uploaded by nds yet
		offset = 1
		t_start = int(t_now) - duration - offset
		omc_data_obj = cdsutils.getdata(omc_channel_name,duration,t_start)
		pzt_data_obj = cdsutils.getdata(pzt_channel_name,duration,t_start)
		if omc_data_obj is None or pzt_data_obj is None:
			print('empty data_obj returned')
			continue
		omc = omc_data_obj.data
		pzt = pzt_data_obj.data

		xdata = np.linspace(t_start,t_start+10,len(omc))
		ax1.clear()
		ax2.clear()
		ax3.clear()
		ax1.plot(xdata,pzt)
		ax2.plot(xdata,omc)
		sort_ind = np.argsort(pzt)
		ax3.plot(pzt[sort_ind],omc[sort_ind])
		plt.pause(0.5) # pause might not be necessary
	return

	
