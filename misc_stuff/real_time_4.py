# plots omc dcpd vs time 
# and pzt vs time
# and omc vs pzt
# and low-passed omc vs pzt

import cdsutils
import matplotlib.pyplot as plt
import time
import numpy as np
from numpy import fft
from subprocess import check_output
from peak_finder import peakdet

#channel_name = 'H1:ASC-AS_B_DC_NSUM_OUT16'
omc_channel_name = 'H1:OMC-DCPD_SUM_OUT_DQ'
#omc_channel_name = 'H1:OMC-PZT2_EXC'
pzt_channel_name = 'H1:OMC-PZT2_EXC'
#pzt_channel_name = 'H1:OMC-PZT2_OUT'

def lorentzian(fwhm,x):
    return 0.5/np.pi*fwhm/(x**2+(0.5*fwhm)**2)

def sinc(fwhm,x):
    return np.sinc(x/fwhm)

def myconv(u,v):
	u_fft = np.fft.fft(u)
	v_fft = np.fft.fft(v)
	return np.fft.fftshift(np.fft.ifft(u_fft*v_fft))
	

def lets_go():
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(411)
	ax2 = fig1.add_subplot(412)
	ax3 = fig1.add_subplot(413)
	ax4 = fig1.add_subplot(414)
	plt.show(block=False)
	while True:
		t_now = check_output(['tconvert', 'now'])
		# carefully chosen as pzt excitation period to avoid 
		# multiple omc vals at a single pzt val
		duration = 10 
		# grab one second before current time because the current
		# second might not have been uploaded by nds yet
		offset = 1
		t_start = int(t_now) - duration - offset
		omc_data_obj = cdsutils.getdata(omc_channel_name,duration,t_start)
		pzt_data_obj = cdsutils.getdata(pzt_channel_name,duration,t_start)
		if omc_data_obj is None or pzt_data_obj is None:
			print('empty data_obj returned')
			continue
		omc = omc_data_obj.data
		pzt = pzt_data_obj.data

		sort_ind = np.argsort(pzt)
		pzt_sort = pzt[sort_ind]
		omc_sort = omc[sort_ind]

		xdata = np.linspace(t_start,t_start+10,len(omc))

		xconv = np.arange(len(xdata)) - len(xdata)/2.0
		fwhm = 100
		L = lorentzian(fwhm,xconv)
		yconv = myconv(np.sinc(xconv/100),omc_sort)

		#delta = np.max(np.abs(np.gradient(yconv))) * 100		
		delta = 1e-4 # chosen to get some peaks in noise

		peak_ind = peakdet(yconv,delta=delta)

		ax1.clear()
		ax2.clear()
		ax3.clear()
		ax4.clear()
		ax1.plot(xdata,pzt)
		ax2.plot(xdata,omc)
		ax3.plot(pzt_sort,omc_sort)
		ax3.plot(pzt_sort[peak_ind],omc_sort[peak_ind],'x',c='r',ms=6,mew=2)		
		ax4.plot(pzt_sort,yconv)
		ax4.plot(pzt_sort[peak_ind],yconv[peak_ind],'x',c='r',ms=6,mew=2)
		plt.pause(0.1) # it's like plt.draw() + sleep(0.1)
	return

	
