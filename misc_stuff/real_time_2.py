# plots omc dcpd vs time 
# and pzt vs time in a single window

import cdsutils
import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output

channel_name = 'H1:ASC-AS_B_DC_NSUM_OUT16'
omc_channel_name = 'H1:OMC-DCPD_SUM_OUT_DQ'
pzt_channel_name = 'H1:OMC-PZT2_EXC'

def lets_go():
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(211)
	ax2 = fig1.add_subplot(212)
	plt.show(block=False)
	while True:
		t_now = check_output(['tconvert', 'now'])
		duration = 10 # seconds
		offset = 1
		t_start = int(t_now) - duration -offset
		omc_data_obj = cdsutils.getdata(omc_channel_name,duration,t_start)
		pzt_data_obj = cdsutils.getdata(pzt_channel_name,duration,t_start)
		if omc_data_obj is None or pzt_data_obj is None:
			print('empty data_obj returned')
			continue
		omc = omc_data_obj.data
		pzt = pzt_data_obj.data
		print(len(omc),len(pzt))
		xdata = np.linspace(t_start,t_start+10,len(omc))
		ax1.clear()
		ax2.clear()
		ax1.plot(xdata,pzt)
		ax2.plot(xdata,omc)
		plt.pause(0.5)
	return

	
