import matplotlib
matplotlib.use('TkAgg')

import numpy as np
import numpy.random
from matplotlib.backends import backend_tkagg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import time
import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
    from Tkinter import messagebox
    import Queue as queue
else:
    import tkinter as Tk
    from tkinter import messagebox
    import queue

import threading

root = Tk.Tk()
root.wm_title("Embedding in TK")
root.alive=True

fig1 = Figure(figsize=(5, 4), dpi=100)
ax1_1 = fig1.add_subplot(211)
ax1_2 = fig1.add_subplot(212)
line1_1, = ax1_1.plot([],[])
line1_2, = ax1_2.plot([],[])

def ayy():
    
    while True:
        x = np.arange(100)
        y = np.random.rand(100)
        
        line1_1.set_data(x,y)
        ax1_1.relim()
        ax1_1.autoscale()
        
        # fig1 is a Tk widget so the main thread has to update it
        root.on_main_thread(fig1.canvas.draw)
        
        # time.sleep(1)
        
#
  
# a tk.DrawingArea
canvas = FigureCanvasTkAgg(fig1,master=root)
# canvas.draw()
canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1) # this inserts the plot as a Tk widget
  
def toggle_selector(event):
    print(event)
  
def pop():
    top = Tk.Toplevel()
    canvas = FigureCanvasTkAgg(fig1,master=top)
    # backend_tkagg.new_figure_manager_given_figure(0, fig1)
    fig1.canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1) # insert the canvas as a Tk widget
    fig1.canvas.mpl_connect('key_press_event', toggle_selector)
     
button = Tk.Button(master=root, text='pop', command=pop).pack()
  

def oi():
    t = threading.Thread(target = lambda : ayy())
    # setting thread as daemon allows the thread to continue executing after the caller is killed
    # this will generally let the thread terminate more gracefully
    t.daemon = True
    t.start()

root.after(1,oi)

q = queue.Queue(maxsize=1)

def on_main_thread(func):
    q.put(func)

def check_queue():
    while True:
        try:
            task = q.get(block=False)
            # print('got task ', task)
            # if q is full it will raise queue.Empty
        except queue.Empty:
            break
        else:
            # print('running task')
            # execute task() when the gui is idle
            root.after_idle(task)
    root.after(10, check_queue)
    
# globals.on_main_thread = on_main_thread
root.check_queue = check_queue
root.on_main_thread = on_main_thread

root.after(2, root.check_queue)

def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.alive=False
        time.sleep(0.5)
        root.destroy()

root.protocol("WM_DELETE_WINDOW", on_closing)

root.mainloop()


# ayy()