# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

#import matplotlib.pyplot as plt
# import scipy.signal
import numpy as np
from subprocess import check_output
import matplotlib
matplotlib.use("TkAgg") # needs to be there
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends import backend_tkagg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.widgets import RadioButtons, RectangleSelector
from peak_finder import peakdet
from OMC_scan_funs_no_pandas_v2 import hamming_wn, brick_lowpass, hann_wn

import time
import threading
import concurrent.futures
import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
    import Queue as queue
else:
    import tkinter as Tk
    import queue

omc_channel = np.load('./omc.npy')
pzt_channel = np.load('./pzt.npy')

t1 = 1208580738
t2 = 1208582718

Fs = 2**14

time_channel = np.linspace(t1,t2,len(pzt_channel))
time_channel2 = np.linspace(t1,t2,len(omc_channel))

# pzt channel is downampled, reinterpolate
pzt_channel2 = np.interp(time_channel2,time_channel,pzt_channel)

print(len(pzt_channel2),len(omc_channel))

class channel:
    
    def __init__(self,stream):
        self.data = stream
        self.time = t1
        self.s1 = 200*Fs
    
    def getdata(self,duration,t_pull=1,t_start=None):
        self.s2 = self.s1 + Fs*duration
        self.time += duration
        grab = self.data[self.s1:self.s2]
        self.s1 += t_pull*Fs
        return grab

FSR=2.649748e+08

class globals_class:
    def __init__(self):
        self.s1 = 200*Fs

def lets_go():
    
    omc_chan = channel(omc_channel)
    pzt_chan = channel(pzt_channel2)
    
    globals = globals_class()
    globals.alive = True
    globals.duration = 12
    globals.q = 8
    globals.pzt_lim = [-10,120]
    globals.omc_lim = [1e-5,0.6]
    globals.yscale = 'linear'
    globals.lowpass = False
    globals.pzt_filter = True
    globals.kill_time = 1
    globals.dark_level = -2e-4
    globals.draw_speed = 0.5
    globals.valid_boxes = False
    globals.m_range = 500
    
    # globals.fig1 = fig1
    
    globals.zeroth = globals_class()
    globals.zeroth.x = [0,0]
    globals.zeroth.y = [0,0]
    
    globals.second = globals_class()
    globals.second.x = [0,0]
    globals.second.y = [0,0]
    
    todict = lambda **kwargs: kwargs
    globals.todict = todict
    def submit():
        text = e.get()
        in_dict = eval('globals.todict('+text+')')
        for key in in_dict:
            setattr(globals, key, in_dict[key])
            
    def set_yscale():
        label = str(yaxis_var.get())
        globals.yscale=label
        
    def set_lowpass():
        label = str(lowpass_var.get())
        if label == 'lowpass':
            globals.lowpass=True
        else:
            globals.lowpass=False
           
    def set_pzt_filter():
        label = str(pzt_var.get())
        if label == 'filter':
            globals.pzt_filter=True
        else:
            globals.pzt_filter=False
            
    def aquire_00():
        text = str(e_00.get())
        in_dict = eval('globals.todict('+text+')') 
        for key in in_dict:
            setattr(globals.zeroth, key, in_dict[key])
        return
        
    def aquire_02():
        text = str(e_02.get())
        in_dict = eval('globals.todict('+text+')') 
        for key in in_dict:
            setattr(globals.second, key, in_dict[key])
        return
        
    def destroy():
        globals.alive = False
        root.destroy()
        
    def leftKey(event):
        print("Left key pressed")

    def rightKey(event):
        print("Right key pressed")
        
    fig0 = Figure()
    fig1 = Figure()
        
    def pop_0():
        win0 = Tk.Toplevel(master=root)    
        canvas0 = FigureCanvasTkAgg(fig0,master=win0)
        fig0.canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
    
    def pop_1():
        win1 = Tk.Toplevel(master=root)  
        canvas01 = FigureCanvasTkAgg(fig1,master=win1)
        fig1.canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
    
    root = Tk.Tk()
    root.grid()
    
    root.bind('<Left>', leftKey)
    root.bind('<Right>', rightKey)
    
    r = 0
    
    e_00 = Tk.Entry(master=root,width=35)
    e_00.grid(row=r,column=0)
    e_00.insert(0,'x=[85,90],y={0}'.format(globals.omc_lim))
    
    r+=1
    
    b_00 = Tk.Button(master=root, text='aquire', command=aquire_00).grid(row=r,column=0)
    
    r+=1
    
    e_02 = Tk.Entry(master=root,width=35)
    e_02.grid(row=r,column=0)
    e_02.insert(0,'x=[85,90],y={0}'.format(globals.omc_lim))
    
    r+=1
    
    b_02 = Tk.Button(master=root, text='aquire', command=aquire_02).grid(row=r,column=0)
    
    r+=1
    
    pzt_var = Tk.StringVar(master=root)
    pzt_var.set('filter') # select default option
    set_pzt_filter()

    rb_pzt1 = Tk.Radiobutton(master=root, text="filter", variable=pzt_var, value='filter',command=set_pzt_filter).grid(row=r,column=0)
    rb_pzt2 = Tk.Radiobutton(master=root, text="no filter", variable=pzt_var, value='no filter',command=set_pzt_filter).grid(row=r,column=1)
    e_pzt = Tk.Entry(master=root)
    e_pzt.grid(row=r,column=2)
    e_pzt.insert(0, 'NA')
    
    r+=1
    
    yaxis_var = Tk.StringVar(master=root)
    yaxis_var.set('linear') # select default option
    set_yscale()

    rb_yax1 = Tk.Radiobutton(master=root, text="semilogy", variable=yaxis_var, value='semilogy',command=set_yscale).grid(row=r,column=0)
    rb_yax2 = Tk.Radiobutton(master=root, text="linear", variable=yaxis_var, value='linear',command=set_yscale).grid(row=r,column=1)
    
    r+=1
    
    lowpass_var = Tk.StringVar(master=root)
    lowpass_var.set('no lowpass') # select default option
    set_lowpass()

    rb_low1 = Tk.Radiobutton(master=root, text="lowpass", variable=lowpass_var, value='lowpass',command=set_lowpass).grid(row=r,column=0)
    rb_low2 = Tk.Radiobutton(master=root, text="no lowpass", variable=lowpass_var, value='no lowpass',command=set_lowpass).grid(row=r,column=1)
    
    r+=1
    
    initial_text = 'duration={0},pzt_lim={1},omc_lim={2},q={3},kill_time={4},dark_level={5},draw_speed={6},m_range={7}'\
    .format(globals.duration,globals.pzt_lim,globals.omc_lim,globals.q,globals.kill_time,globals.dark_level,globals.draw_speed,globals.m_range)
    
    e = Tk.Entry(master=root,width=1000)
    e.grid(row=r,column=0,columnspan=4)
    e.insert(0, initial_text)
    
    r+=1
    
    quit_button = Tk.Button(master=root, text='Kill', command=destroy).grid(row=r,column=0)
    submit_button = Tk.Button(master=root, text='Submit', command=submit).grid(row=r,column=1)
    pop0_button = Tk.Button(master=root, text='pop 0', command=pop_0).grid(row=r,column=2)
    pop1_button = Tk.Button(master=root, text='pop 1', command=pop_1).grid(row=r,column=3)
    
    n_rows = r
    n_cols = 5
    for x in range(n_cols):
        root.grid_columnconfigure(x,weight=1)
    
    for x in range(n_rows):
        root.grid_rowconfigure(x,weight=1)
    
    root.geometry('{}x{}'.format(640, 480))
    
    def kill_by_grad(t,pzt):
        kill_time = globals.kill_time
        dt = t[1]-t[0] # seconds/samples
        kill_samples = int(np.round(kill_time/dt)) # samples
        # kill_samples = 10000
        pzt_grad = np.gradient(pzt)/dt
        good_ind = []
        
        if kill_samples == 0:
            # all indices are good
            good_ind = list(range(len(pzt)))
        else:
            # build a list of good ind by jumping by kill_samples if gradient is too negative
            jj = 0
            for i in range(len(pzt)):
                if jj > len(pzt)-1:
                    break
                # print(jj)
                grad = pzt_grad[jj]
                if grad > -1*globals.pzt_lim[1]:
                    good_ind.append(jj)
                    jj += 1
                else:
                    jj += kill_samples
        
        return good_ind
    
    mismatches = []
    t_mismatches = []
    t_start = t1 + 120
    
    q = queue.Queue(maxsize=5)

    def on_main_thread(func):
        q.put(func)

    def check_queue():
        while True:
            try:
                task = q.get(block=False)
                # print('got task ', task)
                # if q is full it will raise queue.Empty
            except queue.Empty:
                # print('empty queue')
                break
            else:
                # print('running task')
                # execute task() when the gui is idle
                root.after_idle(task)
        # print('scheduling next queue check')
        root.after(10, check_queue)
        
    globals.on_main_thread = on_main_thread
    root.check_queue = check_queue
    
    # executor = concurrent.futures.ThreadPoolExecutor(8)
    
    # in Tk looping works weirdly
    # the mainloop() will block further execution, so you need to call after(t,fun)
    # before mainloop() which will execute fun t ms after mainloop()
    # then before you return from fun you call after(t,fun) again and create
    # perpetual loop of renewed after(t,fun). Very weird.
    
    pop_0()
    pop_1()
       
    def ayy(t_start):
    
        def crop_by_pzt(dc,pzt_lims):
            
            mask = (dc['pzt'] > pzt_lims[0]) & (dc['pzt'] < pzt_lims[1])
            
            dc2 = {}
            for key in dc:
                dc2[key] = dc[key][mask]
            
            return dc2
        
        def RS1_event(eclick, erelease):
            'eclick and erelease are the press and release events'
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            print("(%3.2f, %3.2f) --> (%3.2f, %3.2f)" % (x1, y1, x2, y2))
            print(" The button you used were: %s %s" % (eclick.button, erelease.button))
            globals.zeroth.x = [x1,x2]
            globals.zeroth.y = [y1,y2]
            def update_coords_entry():
                print('!\n!\n!')
                e_00.delete(0,Tk.END)
                e_00.insert(0,'x=[%3.2f,%3.2f],y=[%3.2f,%3.2f]' % (x1,x2,y1,y2))
            
            print('!\n!\n!')
            # globals.on_main_thread(update_coords_entry)
            q.put(update_coords_entry)
            toggle_selector.RS1.set_active(False)
            toggle_selector.RS1.set_visible(True)
            
        def RS2_event(eclick, erelease):
            'eclick and erelease are the press and release events'
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            print("(%3.2f, %3.2f) --> (%3.2f, %3.2f)" % (x1, y1, x2, y2))
            print(" The button you used were: %s %s" % (eclick.button, erelease.button))
            globals.second.x = [x1,x2]
            globals.second.y = [y1,y2]
            def update_coords_entry():
                e_02.delete(0,Tk.END)
                e_02.insert(0,'x=[%3.2f,%3.2f],y=[%3.2f,%3.2f]' % (x1,x2,y1,y2))
                
            globals.on_main_thread(update_coords_entry)
            toggle_selector.RS2.set_active(False)
            toggle_selector.RS2.set_visible(True)
                    
        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['A', 'a'] and not toggle_selector.RS1.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS1.set_active(True)
            if event.key in ['S', 's'] and toggle_selector.RS1.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS1.set_active(False)
            if event.key in ['Z', 'z'] and not toggle_selector.RS2.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS2.set_active(True)
            if event.key in ['X', 'x'] and not toggle_selector.RS2.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS2.set_active(False)
            if event.key in ['D', 'd']:
                toggle_selector.RS1.set_active(False)
                toggle_selector.RS2.set_active(False)
                toggle_selector.RS1.set_visible(False)
                toggle_selector.RS2.set_visible(False)
                globals.zeroth.x = [0,0]
                globals.zeroth.y = [0,0]
                globals.second.x = [0,0]
                globals.second.y = [0,0]
        
        print('figures created')
        
        ax0_1 = fig0.add_subplot(211)
        # ax0_1.set_xlabel('time [seconds]')
        ax0_1.set_ylabel('PZT voltage [counts]') 
        ax0_2 = fig0.add_subplot(212)
        ax0_2.set_xlabel('time [seconds]')
        ax0_2.set_ylabel('OMC DCPD [counts]') 
        
        line0_1, = ax0_1.plot([],[])
        line0_2, = ax0_2.plot([],[])
        line0_2_2, = ax0_2.plot([],[])
        
        ax1_1 = fig1.add_subplot(211)
        ax1_1.set_xlabel('PZT voltage [counts]')
        ax1_1.set_ylabel('OMC DCPD [counts]') 
        ax1_2 = fig1.add_subplot(212)
        ax1_2.set_xlabel('time [seconds]')
        ax1_2.set_ylabel('$P_2\ / \ P_1 $')
        
        line1_1, = ax1_1.plot([],[])
        line1_2, = ax1_2.plot([],[])  

        # remove all default mpl keybindings for fig1 (not necessary unless made by pyplot)
        # fig1.canvas.mpl_disconnect(fig1.canvas.manager.key_press_handler_id)  
        
        toggle_selector.RS1 = RectangleSelector(ax1_1, RS1_event,
                                       drawtype='box',
                                       button=[1, 3],  # don't use middle button
                                       spancoords='pixels')
        toggle_selector.RS2 = RectangleSelector(ax1_1, RS2_event,
                                       drawtype='box',
                                       button=[1, 3],  # don't use middle button
                                       spancoords='pixels')                                       
        toggle_selector.RS1.set_active(False)                                       
        toggle_selector.RS2.set_active(False)
        
        fig1.canvas.mpl_connect('key_press_event', toggle_selector)
        
        fig2 = Figure()
        fig2.set_size_inches(2,2)
        ax2_0 = fig2.add_subplot(111)
        line2_0, = ax2_0.plot([],[])
        line2_0_2, = ax2_0.plot([],[])

        plot2 = Tk.Frame(root)
        plot2.grid(row=0,column=2,rowspan=4)
        canvas2 = FigureCanvasTkAgg(fig2, master=plot2)  # A tk.DrawingArea.
        canvas2.get_tk_widget().pack( side = Tk.RIGHT, expand=1)
        canvas2.draw()
        
        fig3 = Figure()
        fig3.set_size_inches(2,2)
        ax3_0 = fig3.add_subplot(111)
        line3_0, = ax3_0.plot([],[])
        line3_0_2, = ax3_0.plot([],[])

        plot3 = Tk.Frame(root)
        plot3.grid(row=0,column=3,rowspan=4)
        canvas = FigureCanvasTkAgg(fig3, master=plot3)  # A tk.DrawingArea.
        canvas.get_tk_widget().pack( side = Tk.RIGHT, expand=1)
        canvas.draw()
    
        while globals.alive:
            
            duration = globals.duration
            # grab one second before current time because the current
            # second might not have been uploaded by nds yet
            offset = 1
            # t_start = int(t_now) - duration - offset
                
            omc = omc_chan.getdata(duration) - globals.dark_level
            pzt = pzt_chan.getdata(duration)
            
            t_end = t_start + duration
            t_vec = np.linspace(t_start,t_end,len(omc))
            t_start = t_end
            
            mydecimate = lambda x,q: x[::q]
            
            t_vec = mydecimate(t_vec,globals.q)
            omc = mydecimate(omc,globals.q)
            pzt = mydecimate(pzt,globals.q)
            
            # print('at grad kill')
            
            if globals.pzt_filter:
                good_ind = kill_by_grad(t_vec,pzt)
                
                t_vec = t_vec[good_ind]
                omc = omc[good_ind]
                pzt = pzt[good_ind]
            
            if len(t_vec) < 2:
                # if the block is less than 2 samples then just skip the entire block
                continue
            
            dt = t_vec[1]- t_vec[0]
            
            # print('at filt', len(omc),len(pzt))

            sort_ind = np.argsort(pzt)
            pzt_sort = pzt[sort_ind]
            omc_sort = omc[sort_ind]
            
            # print('at lowpass')
            
            if globals.lowpass:
                omc_filt = brick_lowpass(omc,10,M=50,wn_fun=hann_wn)
            else:
                omc_filt = omc
            omc_filt_sort = omc_filt[sort_ind]

            # data needs to be packaged this way for OMC_scan lib
            dc = {}
            dc['omc'] = omc_filt_sort
            dc['pzt'] = pzt_sort
                
            dc00 = crop_by_pzt(dc,globals.zeroth.x)
            dc02 = crop_by_pzt(dc,globals.second.x)
            
            if (len(dc00['omc']) < 2) or (len(dc02['omc']) < 2):
                # at least one of the selection boxes is empty
                # turn off ratio measurement
                globals.valid_boxes = False
            else:
                # boxes contain valid data
                # take ratio of maxes in each box
                # user is responsible for checking that the max in the box corresponds to the peak
                globals.valid_boxes = True
                               
                pidx_00 = np.argmax(dc00['omc'])
                pidx_02 = np.argmax(dc02['omc'])
                
                a_00 = dc00['omc'][pidx_00]
                a_02 = dc02['omc'][pidx_02]
                
                mismatches.append(a_02/a_00)
                t_mismatches.append(t_end)
                
        # def update_plots():
            
            # print('at plot')
           
            line0_1.set_data(t_vec,pzt)
            line0_1.set_linestyle('')
            line0_1.set_marker('.')
            line0_1.set_ms(1)
            ax0_1.set_xlim(min(t_vec),max(t_vec))
            ax0_1.set_ylim(globals.pzt_lim)
            
            line0_2.set_data(t_vec,omc)
            line0_2.set_linestyle('')
            line0_2.set_marker('.')
            line0_2.set_ms(1)            
            line0_2_2.set_color('red')
            ax0_2.set_xlim(min(t_vec),max(t_vec))
            ax0_2.set_ylim(globals.omc_lim)
            
            line1_1.set_data(dc['pzt'],dc['omc'])
            ax1_1.set_xlim(globals.pzt_lim)
            ax1_1.set_ylim(globals.omc_lim)
            
            line1_2.set_data(t_mismatches[-globals.m_range:],mismatches[-globals.m_range:])
            ax1_2.relim()
            ax1_2.autoscale()
                       
            line2_0.set_data(dc00['pzt'],dc00['omc'])
            # ax2_0.set_xlim(globals.zeroth.x)
            # ax2_0.set_ylim(globals.zeroth.y)
            ax2_0.relim()
            ax2_0.autoscale()
            
            line3_0.set_data(dc02['pzt'],dc02['omc'])
            # ax3_0.set_xlim(globals.second.x)
            # ax3_0.set_ylim(globals.second.y)
            ax3_0.relim()
            ax3_0.autoscale()
            
            if globals.valid_boxes:
                line2_0_2.set_data(dc00['pzt'][pidx_00],dc00['omc'][pidx_00])
                line2_0_2.set_ls('')
                line2_0_2.set_marker('x')
                
                line3_0_2.set_data(dc02['pzt'][pidx_02],dc02['omc'][pidx_02])
                line3_0_2.set_ls('')
                line3_0_2.set_marker('x')
            else:
                line2_0_2.set_data([],[])
                line3_0_2.set_data([],[])
            
            if globals.lowpass:
                line0_2_2.set_data(t_vec,omc_filt)
            
            if globals.yscale == 'semilogy':
                ax0_2.set_yscale('log')
                ax1_1.set_yscale('log')
                ax2_0.set_yscale('log')
                ax3_0.set_yscale('log')
            else:
                ax0_2.set_yscale('linear')
                ax1_1.set_yscale('linear')
                ax2_0.set_yscale('linear')
                ax3_0.set_yscale('linear')
                
            def update_fig(fig,num):
                # print('updating fig',num)
                fig.canvas.draw()
                
            def batch_update():
                update_fig(fig0,0)
                update_fig(fig1,1)
                update_fig(fig2,2)
                update_fig(fig3,3)                
                
            # figures are Tk widgets so they have to be updated on main thread
            # globals.on_main_thread(lambda: update_fig(fig0,0))
            # globals.on_main_thread(lambda: update_fig(fig1,1))
            # globals.on_main_thread(lambda: update_fig(fig2,2))
            # globals.on_main_thread(lambda: update_fig(fig3,3))
            
            # wait for queue to clear before sending more draws to it
            # while not q.empty():
                # time.sleep(globals.draw_speed)
            
            # print('sending plot puts')
            # t2 = threading.Thread(target=batch_update)
            # t2.daemon = True
            # t2.start()
            
            update_fig(fig0,0)
            update_fig(fig1,1)
            update_fig(fig2,2)
            update_fig(fig3,3)
            
            time.sleep(globals.draw_speed)
            
            # q.put(lambda: update_fig(fig0,0))
            # q.put(lambda: update_fig(fig1,1))
            # q.put(lambda: update_fig(fig2,2))
            # q.put(lambda: update_fig(fig3,3))
            
            
            # print('at end')    
            
        return
    
    # print(future.result())
    
    # after() is what allows Tk apps to execute other code
    root.after(1, root.check_queue)
    t = threading.Thread(target = lambda : ayy(t1))
    t.daemon = True
    t.start()
    # future = executor.submit(callback, t1)
    # print(future.exception())
    
    root.mainloop()
    # print('killed')
    # print(future.cancel())
    print(globals.alive)

    return
    
lets_go()