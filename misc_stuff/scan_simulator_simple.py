# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

#import matplotlib.pyplot as plt
import scipy.signal
import numpy as np
from subprocess import check_output
import matplotlib
matplotlib.use("TkAgg") # needs to be there
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox, RadioButtons
from peak_finder import peakdet
from OMC_scan_funs_no_pandas_v2 import crop_data, stage2, stage3, hamming_wn, apply_lowpass_omc, bootstrap_failed, brick_lowpass, hann_wn

omc_channel = np.load('./omc.npy')
pzt_channel = np.load('./pzt.npy')

t1 = 1208580738
t2 = 1208582718

Fs = 2**14

time_channel = np.linspace(t1,t2,len(pzt_channel))
time_channel2 = np.linspace(t1,t2,len(omc_channel))

# pzt channel is downampled, reinterpolate
pzt_channel2 = np.interp(time_channel2,time_channel,pzt_channel)

print(len(pzt_channel2),len(omc_channel))

class channel:
    
    def __init__(self,stream):
        self.data = stream
        self.time = t1
        self.s1 = 200*Fs
    
    def getdata(self,duration,t_pull=1,t_start=None):
        self.s2 = self.s1 + Fs*duration
        self.time += duration
        grab = self.data[self.s1:self.s2]
        self.s1 += t_pull*Fs
        return grab

FSR=2.649748e+08

class globals_class:
    def __init__(self):
        self.OMC_DC_offset = -0.1
        self.s1 = 200*Fs

def lets_go():
    
    fig1 = plt.figure()
    fig2 = plt.figure()
    ax1 = fig1.add_subplot(411)
    ax2 = fig1.add_subplot(412)
    ax3 = fig1.add_subplot(413)
    ax4 = fig1.add_subplot(414)
    plt.show(block=False)
    
    omc_chan = channel(omc_channel)
    pzt_chan = channel(pzt_channel2)
    
    globals = globals_class()
    globals.duration = 12
    globals.q = 8
    globals.pzt_lim = [-10,120]
    globals.omc_lim = [7e-2,0.5]
    globals.OMC_DC_offset = -0.1
    globals.yscale = 'linear'
    globals.lowpass = False
    
    def submit(text):
        todict = lambda **kwargs: kwargs
        in_dict = eval('todict('+text+')')
        for key in in_dict:
            setattr(globals, key, in_dict[key])
            
    def set_yscale(label):
        globals.yscale=label
        
    def set_lowpass(label):
        if label == 'lowpass':
            globals.lowpass=True
        else:
            globals.lowpass=False
        
    initial_text = 'duration={0},pzt_lim={1},omc_lim={2},q={3}'.format(globals.duration,globals.pzt_lim,globals.omc_lim,globals.q)
    axbox = fig2.add_axes([0, 0, 0.8, 0.075])
    text_box = TextBox(axbox, 'Evaluate', initial=initial_text)
    text_box.on_submit(submit)
    
    rax1 = fig2.add_axes([0.05, 0.7, 0.15, 0.15])
    radio1 = RadioButtons(rax1, ('linear','semilogy'))
    radio1.on_clicked(set_yscale)
    
    rax2 = fig2.add_axes([0.22, 0.7, 0.15, 0.15])
    radio2 = RadioButtons(rax2, ('lowpass','no lowpass'),active=1)
    radio2.on_clicked(set_lowpass)
    
    def kill_by_grad(t,pzt):
        kill_time = 1 # seconds
        dt = t[1]-t[0] # seconds/samples
        kill_samples = int(kill_time/dt) # samples
        # kill_samples = 10000
        pzt_grad = np.gradient(pzt)/dt
        good_ind = []
        
        # build a list of good ind by jumping by kill_samples if gradient is too negative
        jj = 0
        for i in range(len(pzt)):
            if jj > len(pzt)-1:
                break
            # print(jj)
            grad = pzt_grad[jj]
            if grad/dt > -10*globals.pzt_lim[1]:
                good_ind.append(jj)
                jj += 1
            else:
                jj += kill_samples
        
        return good_ind

    ts = []
    mismatches = []
    t_start = t1 + 120
    while True:
        duration = globals.duration
        # grab one second before current time because the current
        # second might not have been uploaded by nds yet
        offset = 1
        # t_start = int(t_now) - duration - offset
            
        omc = omc_chan.getdata(duration) - globals.OMC_DC_offset
        pzt = pzt_chan.getdata(duration)
        
        t_end = t_start + duration
        t_vec = np.linspace(t_start,t_end,len(omc))
        t_start = t_end
        
        mydecimate = lambda x,q: x[::q]
        
        t_vec = mydecimate(t_vec,globals.q)
        omc = mydecimate(omc,globals.q)
        pzt = mydecimate(pzt,globals.q)
        
        print('at grad kill')
        
        good_ind = kill_by_grad(t_vec,pzt)
        
        t_vec = t_vec[good_ind]
        omc = omc[good_ind]
        pzt = pzt[good_ind]
        
        dt = t_vec[1]- t_vec[0]
        
        print(len(omc),len(pzt))

        sort_ind = np.argsort(pzt)
        pzt_sort = pzt[sort_ind]
        omc_sort = omc[sort_ind]
        
        if globals.lowpass:
            omc_filt = brick_lowpass(omc,10,M=50,wn_fun=hann_wn)
        else:
            omc_filt = omc
        omc_filt_sort = omc_filt[sort_ind]

        # data needs to be packaged this way for OMC_scan lib
        dc = {}
        dc['omc'] = omc_filt_sort
        dc['pzt'] = pzt_sort
        

        #TODO: define better deltas
        # delta_upper = np.max(dc['omc'])/1.3
        # delta_lower = np.max(dc['omc'])/10
        #delta = 1e-4 # chosen for noise plotting
        
        peak_ind = []
        # peak_ind = peakdet(omc_filt_sort,delta_upper)
        
        # print('at plot')

        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()
        
        ax1.plot(t_vec,pzt,'.')
        ax1.set_ylim(globals.pzt_lim)

        if globals.yscale == 'semilogy':
            ax2.semilogy(t_vec,omc)
            if globals.lowpass:
                ax2.semilogy(t_vec,omc_filt,c='r')
            ax3.semilogy(dc['pzt'],dc['omc'])
            ax3.semilogy(dc['pzt'][peak_ind],dc['omc'][peak_ind],'x',c='r')
        else:
            ax2.plot(t_vec,omc,'.',ms=0.5)
            if globals.lowpass:
                ax2.plot(t_vec,omc_filt,c='r')
            ax3.plot(dc['pzt'],dc['omc'])
            ax3.plot(dc['pzt'][peak_ind],dc['omc'][peak_ind],'x',c='r')
            
        ax2.set_ylim(globals.omc_lim)
        ax3.set_xlim(globals.pzt_lim)
        ax3.set_ylim(globals.omc_lim)

        plt.pause(0.0001) # it's like plt.draw() + sleep(0.1)
    return
