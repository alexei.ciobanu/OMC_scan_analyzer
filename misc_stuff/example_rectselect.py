# import matplotlib
# matplotlib.use('TkAgg')
# import matplotlib.pyplot as plt
# from matplotlib.widgets import  RectangleSelector
# import numpy as np
# import numpy.random
import time
import threading
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.widgets import  RectangleSelector
import numpy as np
import numpy.random
import time

def ayy():
#     import threading
    
    def onselect(eclick, erelease):
        'eclick and erelease are matplotlib events at press and release'
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        print(' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata))
        print(' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata))
        print(' used button   : ', eclick.button)
        toggle_selector.RS.set_active(False)

    def toggle_selector(event):
        print(' Key pressed.')
        if event.key in ['Q', 'q'] and toggle_selector.RS.active:
            print(' RectangleSelector deactivated.')
            toggle_selector.RS.set_active(False)
        if event.key in ['A', 'a'] and not toggle_selector.RS.active:
            print(' RectangleSelector activated.')
            toggle_selector.RS.set_active(True)

    class MyRectangleSelector(RectangleSelector):
        def release(self, event):
            super(MyRectangleSelector, self).release(event)
            self.to_draw.set_visible(True)
            self.canvas.draw()

#######################################################            
            
    fig1 = plt.figure()
    
    ax11 = fig1.add_subplot(211)
    ax12 = fig1.add_subplot(212)
    
    line11, = ax11.plot([], [], lw=2)
    line12, = ax12.plot([], [], lw=2)

    fig2 = plt.figure()
    
    fig2.canvas.mpl_disconnect(fig2.canvas.manager.key_press_handler_id)
    
    ax21 = fig2.add_subplot(211)
    ax22 = fig2.add_subplot(212)
    
    line21, = ax21.plot([], [], lw=2)
    line22, = ax22.plot([], [], lw=2)
    
    # fig2.canvas.mpl_disconnect(fig2.canvas.manager.key_press_handler_id)
    toggle_selector.RS = MyRectangleSelector(ax21, onselect,
                                   drawtype='box',
                                   button=[1, 3],  # don't use middle button
                                   spancoords='pixels')
    fig2.canvas.mpl_connect('key_press_event', toggle_selector)
    toggle_selector.RS.set_active(False)

    plt.show(block=False)   

    ax11.set_xlim([0,100])
    ax11.set_ylim([0,1])
    ax12.set_xlim([0,100])
    ax12.set_ylim([0,1])
    ax21.set_xlim([0,100])
    ax21.set_ylim([0,1])

#########################################################
    
    # fig0 = plt.figure()
    
    # ax0_1 = fig0.add_subplot(211)
    # ax0_2 = fig0.add_subplot(212)
    
    # line0_1, = ax0_1.plot([],[])
    # line0_2, = ax0_2.plot([],[])
    
    # fig1 = plt.figure()
    
    # # remove all default mpl keybindings for fig1
    # fig1.canvas.mpl_disconnect(fig1.canvas.manager.key_press_handler_id)
    
    # ax1_1 = fig1.add_subplot(211)
    # ax1_2 = fig1.add_subplot(212)
    
    # line1_1, = ax1_1.plot([],[])
    # line1_2, = ax1_2.plot([],[])
         
    # toggle_selector.RS = MyRectangleSelector(ax1_1, onselect,
                                   # drawtype='box', useblit=True,
                                   # button=[1, 3],  # don't use middle button
                                   # minspanx=5, minspany=5,
                                   # spancoords='pixels')
                                   
    # toggle_selector.RS.set_active(False)
    # fig1.canvas.mpl_connect('key_press_event', toggle_selector)

    # ax0_1.set_xlim([0,100])
    # ax0_1.set_ylim([0,1])
    # ax0_2.set_xlim([0,100])
    # ax0_2.set_ylim([0,1])
    # ax1_1.set_xlim([0,100])
    # ax1_1.set_ylim([0,1])

######################################################################    
 
    i=0
    while plt.fignum_exists(1):

        time.sleep(0.1)

        #####################################
        x = np.arange(100)
        y = np.random.rand(100)

        line12.set_data(x,np.random.rand(100))
        line11.set_data(x,np.random.rand(100))

        line21.set_data(x,np.random.rand(100))

        ###########################################
        
        # x = np.arange(100)
        # y = np.random.rand(100)

        # line0_1.set_data(x,np.random.rand(100))
        # line0_2.set_data(x,np.random.rand(100))
        
        # line1_1.set_data(x,np.random.rand(100))
        
        plt.pause(1)

        
t = threading.Thread(target = ayy)
t.start()

j = 0
while True:
    j+= 1
    time.sleep(1)
    print(j)