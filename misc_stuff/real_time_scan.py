# TODO: buffer, peak find/label, averaging mode scans, save mismatch vs time data
# TODO: fix zero level offset in OMC DCPD

import cdsutils
import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output
from OMC_scan_funs_no_pandas_v2 import crop_data, stage2, stage3, hamming_wn, apply_lowpass_omc, bootstrap_failed, brick_lowpass, hann_wn, digitize_output

omc_channel_name = 'H1:OMC-DCPD_SUM_OUT_DQ'
pzt_channel_name = 'H1:OMC-PZT2_EXC'
pzt_output = 'H1:OMC-PZT2_OUTPUT'
pzt_out = 'H1:OMC-PZT2_OUT'
pzt_out_DC_OUT_DQ = 'H1:OMC-PZT2_MON_DC_OUT_DQ'

FSR=2.649748e+08

def lets_go():
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(411)
    ax2 = fig1.add_subplot(412)
    ax3 = fig1.add_subplot(413)
    ax4 = fig1.add_subplot(414)
    plt.show(block=False)

    ts = []
    mismatches = []
    while True:
        t_now = check_output(['tconvert', 'now'])
        # carefully chosen as pzt excitation period to avoid 
        # multiple omc vals at a single pzt val
        duration = 1/.05
        # grab one second before current time because the current
        # second might not have been uploaded by nds yet
        offset = 1
        t_start = int(t_now) - duration - offset
        
        OMC_DC_offset = -0.0005
        omc_data_obj = cdsutils.getdata(omc_channel_name,duration,t_start)
        pzt_data_obj = cdsutils.getdata(pzt_channel_name,duration,t_start)
        if omc_data_obj is None or pzt_data_obj is None:
            print('empty data_obj returned')
            continue
        omc = omc_data_obj.data - OMC_DC_offset 
        pzt = pzt_data_obj.data

        sort_ind = np.argsort(pzt)
        pzt_sort = pzt[sort_ind]
        omc_sort = omc[sort_ind]
        
        # data needs to be packaged this way for OMC_scan lib
        dc = {}
        dc['omc'] = omc_sort
        dc['pzt'] = pzt_sort
        
        print('at_digitize')
        
        #dc = digitize_output(dc,target='pzt',N_bins=40000)
        dc = crop_data(dc,keep_range=[0.08,1])
        
        dc_filt = {}
        omc_filt = brick_lowpass(dc['omc'],35,M=75,wn_fun=hann_wn)
        dc_filt['omc'] = omc_filt
        dc_filt['pzt'] = dc['pzt']
        
        if np.max(np.abs(dc['pzt'])) > 50:
            print('ay')
            #need to round to nearest power of 2
            N = len(dc['pzt'])
            N_new = int(2**(np.floor(np.log2(N))))
            #dc = digitize_output(dc,target='pzt',N_bins=N_new,verbose=False)

        yrange = np.max(omc_filt) - np.min(omc_filt)
        #TODO: define better deltas
        delta_upper = yrange/1.2
        delta_lower = yrange/100
        #delta = 1e-4 # chosen for noise plotting
        
        print('at try')
        try:
            #TODO: allow stage2 to fail (ie return None)
            dc2 = stage2(dc_filt,delta_lower=delta_lower,delta_upper=delta_upper,use_matched_filt=True
            ,use_only_linear_freq=True)
            
            #TODO: make stage3 return peak locs/labels (useful for visually tracking algo errors)
            mismatch = stage3(dc2,delta = delta_lower,verbose=False,confidence_cutoff=1e-1)
            
            
            #TODO: write out these two in append mode to some file
            ts.append(t_now)
            mismatches.append(mismatch)
        except bootstrap_failed:
            # thats fine. just try again next iteration
            dc2 = {}
            dc2['freq'] = []
            dc2['omc'] = []
            
        xdata = np.linspace(t_start,t_start+10,len(omc))

        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()
        ax1.plot(xdata,pzt)
        ax2.plot(xdata,omc)
        #ax2.plot(xdata,omc_filt,c='r')
        ax3.semilogy(dc_filt['pzt'],dc_filt['omc'])
        #TODO: plot peak locs here
        # ax3.plot(pzt_sort[peak_ind],omc_sort[peak_ind],'x',c='r',ms=6,mew=2)		
        ax4.plot(ts,mismatches)
        plt.pause(0.1) # it's like plt.draw() + sleep(0.1)
    return
