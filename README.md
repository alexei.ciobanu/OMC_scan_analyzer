# OMC Scanalyzer

## Usage

Run main.py in OMC_scanalyzer_dev. For details see documentation.

## Dependencies

* nds2 - Currently confirmed to work on LHO CDS machines. Needs to be tested for LLO.
* numpy
* matplotlib
* tkinter

Developed and tested on whatever versions of those libraries were at LHO.

## Contact

For issues with the code or general inquires. You can reach me at alexei.ciobanu@adelaide.edu.au